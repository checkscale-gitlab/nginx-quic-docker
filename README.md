# nginx-quic-docker

This repository holds the Dockerfile and associated files needed to build [dasskelett/nginx-quic](https://hub.docker.com/r/dasskelett/nginx-quic),
a nginx image compiled from the [nginx-quic branch](https://hg.nginx.org/nginx-quic) with support for HTTP/3 and QUIC.
The image is based on `archlinux:base-devel` to make use of the [nginx-quic](https://aur.archlinux.org/packages/nginx-quic/) AUR package.
The Dockerfile is loosely oriented on the [Dockerfile](https://github.com/nginxinc/docker-nginx/blob/master/mainline/alpine/Dockerfile) for the official [nginx:alpine](https://hub.docker.com/_/nginx) image.

Containers will listen for HTTPS (HTTP/1.1 via TLS, HTTP/2 via TLS, HTTP/3 via QUIC) connections on port 443 by default.
The image generates a self-signed certificate at container start for this reason,
at `/etc/nginx/key.pem` (configurable using `$KEY_LOCATION`) and `/etc/nginx/cert.pem` (`$CERT_LOCATION`), _only_ if both these files do not exist yet.
To use RSA instead of ECDSA / P-256, set `$CERT_USE_EC` to `false`.

Keep in mind that HTTP/3+QUIC support in nginx is still WIP.
Check [this blog post](https://www.nginx.com/blog/our-roadmap-quic-http-3-support-nginx/) for the latest status update.

[Dockerfile/Source](https://gitlab.com/DasSkelett/nginx-quic-docker/-/blob/master/Dockerfile)

## Usage

Docker command line:
```bash
docker run -d --name nginx-quic -p [::]:80:80 -p [::]:443:443/tcp -p [::]:443:443/udp dasskelett/nginx-quic:latest
```

docker-compose.yml:
```yaml
services:
  nginx:
    image: dasskelett/nginx-quic:latest
    restart: always
    ports:
      - "80:80"
      - "443:443/tcp"
      - "443:443/udp"
    volumes:
      - /var/www/:/usr/share/nginx/
      - ./nginx/etc/nginx.conf:/etc/nginx/nginx.conf:ro
      - ./nginx/etc/conf.d/:/etc/nginx/conf.d/:ro
      - /etc/letsencrypt/live/domain.tld/fullchain.pem:/etc/nginx/cert.pem:ro
      - /etc/letsencrypt/live/domain.tld/privkey.pem:/etc/nginx/cert.key:ro
```


For more information about how to use HTTP/3 and QUIC with nginx, see the official [README](https://hg.nginx.org/nginx-quic/file/tip/README).
